# NPM NOTES!

# npm is a package (library) manager for javascript and you use it to download packages into your project for use
  # NOTE: it is the DEFAULT package manager system for node. Many languages have their own library/package management system that are different from others
# What are packages?
  # packages are unified collections of code - or more formally referred to as libraries in programming - that you can download and use
  # they are valuable because you DO NOT have to write everything on your own! You can reuse code that others have written to simplify development in a number of ways

# to start using npm in a folder where you are working on a project:
npm init
# this is going to create a package.json
# npm init ONLY needs to be run ONCE during the creation of a package.json file. 
  # if you download a project with a package.json, you DO NOT need to run this command
package.json package.json.lock
# package.json
  # file manages your dependencies (libraries/packages) 
  # they are called dependencies because your project DEPENDS on them :)
  # this file has a section called dependencies which contains the name and version of dependencies for the project
      # these dependencies are listed as nothing more than text. The only significant thing about these dependencies is that they tell npm what is needed 
      # NOTE: if just running npm init, there may not be this section yet until you install some dependencies
# package.json.lock
  # a recent addition to npm which LOCKS down relations between dependencies in the package.json and the downloaded libraries in node_modules
  # it provides many important features for handling dependencies in the background quickly and safely

# to install a package run:
npm install 
# this installs a package but DOES NOT update the package.json dependencies
# BUT the above file does NOT update the package.json file!!!
  # this is important because in a project you only share the package.json files to show what is needed, so other people will be able to download what is needed
# to fix this: 
npm install --save
# this will create a new dependency entry in your package.json file
  # this will be how you should most often install packages, but we might discuss other ways later

# Why is this important? Because when you install packages you create the node_modules folder where the libraries/packages are actually downloaded to be used
node_modules
# folder where all of the downloaded dependencies are stored


# What should be on my github repository when working with npm???
  # ONLY UPLOAD: package.json and package.json.lock
  # DO NOT UPLOAD!!!! - node_modules
  # take a look at the example bundled .gitignore file as an example

  # Create a .gitignore file in your repository and add node_modules to it to make sure that git never uploads them

# What do I do when I download a project with a package.json file at work, when working in a group, or outside of class?
# run:
npm install
# This will go look at the dependencies section of the package.json that we talked about ealier and install everything listed there
# this is how people share projects which use npm and download all the same dependencies!



# Bonus section: yarn
  # we are probably not going to use this in class but you will likely see it online 
  # its basically another tool that you can use to WORK WITH npm
  # in this way its not really any different, just another tool that does all the same things for you
  # its commands look different too, but do not worry, its just doing npm things a little differently
