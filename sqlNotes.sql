-- Will destroy and delete the database if it already exists
-- You will loose all the data from the database if it exists,
DROP DATABASE IF EXISTS Foo;

CREATE DATABASE Foo;

-- to select the right database for the sql file in mysql workbench, add this:
use Foo;

-- to create a table in a database 
CREATE TABLE Users(
	id Integer,
    firstName VARCHAR(255),
    lastName VARCHAR(255)
);

-- to insert into a database
insert into Users(id, firstName, lastName) 
values (1, "test", "tet"), (2, "et", "sdaf");

-- to update a database
UPDATE Users set email = "somethingElse@whatever.com" 
where id = 2;

-- to delete from a database 
DELETE FROM Users where firstName = "sampleName";

-- collectively these operations are referred to as CRUD (Create, Read, Update, Delete)